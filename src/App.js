import React, { useState, useEffect } from "react";
import { Button } from 'react-bootstrap';


function App() {
  const [dataInfos, setDataInfos] = useState("");
  const [isSIREN, setIsSIREN] = useState("false");
  const [isSIRET, setIsSIRET] = useState("false");
  const [valueInput, setValueInput] = useState("");

  const getAPIResult = (value) => {
    var httpsAdress = "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/?etat_administratif=A&"
    if(isSIREN === "true") {
      httpsAdress += "siren="
    } else {
      httpsAdress +="siret="
    }
    fetch(httpsAdress + value)
                  .then((res) => res.json())
                  .then((json) => {
                    console.log(json)
                    if(json.message){
                      setDataInfos("Bad request, no company corresponding to: " + value)
                    } else {
                      var data = "Nom: " + json.etablissements[0].unite_legale.nom + 
                                "\nPrenom: " + json.etablissements[0].unite_legale.prenom_usuel +
                                "\nSexe: " + json.etablissements[0].unite_legale.sexe +
                                "\nAdresse Siege: " + json.etablissements[0].unite_legale.etablissement_siege.geo_adresse +
                                "\nDate creation: " + json.etablissements[0].date_creation +
                                "\nSIRET number: " + json.etablissements[0].siret + 
                                "\nSIRET number: " + json.etablissements[0].siren + 
                                "\nCategorie entreprise: " + json.etablissements[0].unite_legale.categorie_entreprise + 
                                "\nCategorie juridique: " + json.etablissements[0].unite_legale.categorie_juridique
                      setDataInfos(data)
                    }          
                  })
  }

  const handleNewValueInput = () => {
    if(!/^\d+$/.test(valueInput)){
      setDataInfos("Le numero de SIRET est composé de 14 chiffres\nLe numéro de SIREN est composé de 9 chiffres")
      setIsSIREN("false")
      setIsSIRET("false")
    } else {
      if(valueInput.length === 14){   
        setIsSIREN("false")
        setIsSIRET("true")
        getAPIResult(valueInput)
      } else if(valueInput.length === 9){
        setIsSIRET("false")
        setIsSIREN("true")
        getAPIResult(valueInput)
      } else {
        setDataInfos("Le numero de SIRET est composé de 14 chiffres\nLe numéro de SIREN est composé de 9 chiffres")
        setIsSIREN("false")
        setIsSIRET("false")
      }
    }
  }


  return (
    <div className="App">
      <p>Is SIREN number: {isSIREN}</p>
      <p>Is SIRET number: {isSIRET}</p>
      <div>
        <input value={valueInput} onChange={e => setValueInput(e.target.value)}/>
        <Button type="submit" onClick={() => handleNewValueInput()}>Check</Button>
      </div>
      <p>Length: {valueInput.length}</p>
      <p>{dataInfos}</p>
      </div>
  );
}

export default App;
